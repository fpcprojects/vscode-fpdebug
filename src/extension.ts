/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation / CNOC. All rights
 * reserved.
 *--------------------------------------------------------*/

'use strict';

import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken, DebugAdapterTracker, DebugAdapterTrackerFactory, DebugSession, OutputChannel, commands } from 'vscode';
import * as path from 'path';

export function activate(context: vscode.ExtensionContext) {

	commands.registerCommand('extension.fpDebug.executableExtension', () => {
		return (process.platform === "win32")?".exe":""
	  });

	// register a configuration provider for 'mock' debug type
	const provider = new MockConfigurationProvider();
	context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('fpDebug', provider));

	const factory: vscode.DebugAdapterDescriptorFactory = new MockDebugAdapterDescriptorFactory();
	context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('fpDebug', factory));

	const tracker = new FpDebugAdapterTrackerFactory();
	context.subscriptions.push(vscode.debug.registerDebugAdapterTrackerFactory('fpDebug', tracker));
}

export function deactivate() {
	// nothing to do
}

class MockConfigurationProvider implements vscode.DebugConfigurationProvider {

	/**
	 * Massage a debug configuration just before a debug session is being launched,
	 * e.g. add all missing attributes to the debug configuration.
	 */
	resolveDebugConfiguration(folder: WorkspaceFolder | undefined, config: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration> {

		// if launch.json is missing or empty
		if (!config.type && !config.request && !config.name) {
			const editor = vscode.window.activeTextEditor;
			if (editor && editor.document.languageId === 'pascal') {
				config.type = 'fpDebug';
				config.name = 'Launch';
				config.request = 'launch';
				config.program = '${file}';
			}
		}

		if (!config.program) {
			return vscode.window.showInformationMessage("Cannot find a program to debug").then(_ => {
				return undefined;	// abort launch
			});
		}

		return config;
	}

	resolveDebugConfigurationWithSubstitutedVariables?(folder: WorkspaceFolder | undefined, debugConfiguration: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration> {
		// if launch.json is missing or empty
		if ((path.extname(debugConfiguration.program) == '.pas') || (path.extname(debugConfiguration.program) == '.pp') || (path.extname(debugConfiguration.program) == '.lpr')) {
			// Remove the .pas extension and replace it with the default extension for an executable
			debugConfiguration.program=path.join(path.dirname(debugConfiguration.program), path.basename(debugConfiguration.program, path.extname(debugConfiguration.program)) + '')
		}

		return debugConfiguration;
	}
}

class FpDebugAdapterTracker implements DebugAdapterTracker {
  private readonly enabled: boolean = false;
  private orange;

  constructor(private readonly session: DebugSession, createOutputChannelProc: { (): vscode.OutputChannel; }) {
    this.enabled = this.session.configuration.log as boolean;
    if (this.enabled) {
      this.orange = createOutputChannelProc();
    }
  }

  public onWillStartSession() {
    this.log(`Starting Session:\n${this.stringify(this.session.configuration)}\n`);
  }

  public onWillReceiveMessage(message: any) {
    this.log(`Client --> Adapter:\n${this.stringify(message)}\n`);
  }

	public onDidSendMessage(message: any) {
		this.log(`Client <-- Adapter:\n${this.stringify(message)}\n`);
	}

  public onWillStopSession() {
    this.log('Stopping Session\n');
  }

  public onError(error: Error) {
    this.log(`Error:\n${this.stringify(error)}\n`);
  }

  public onExit(code: number | undefined, signal: string | undefined) {
    this.log(`Exit:\nExit-Code: ${code ? code : 0}\nSignal: ${signal ? signal : 'none'}\n`);
  }

  private log(message: string) {
    if (this.enabled) {
      this.orange.appendLine(`${message}`);
    }
  }

  private stringify(data: Error | DebugConfiguration | any ) {
    return JSON.stringify(data, null, 4);
  }
}

export class FpDebugAdapterTrackerFactory implements DebugAdapterTrackerFactory {

  private outputChannel: OutputChannel;

  public createDebugAdapterTracker(session: DebugSession): ProviderResult<DebugAdapterTracker> {
    return new FpDebugAdapterTracker(session, () => {
      if (!this.outputChannel) {
        this.outputChannel = vscode.window.createOutputChannel("FpDebug DAB communication");
      }
      return this.outputChannel
    });
  }
}


class MockDebugAdapterDescriptorFactory implements vscode.DebugAdapterDescriptorFactory {

	createDebugAdapterDescriptor(session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): vscode.ProviderResult<vscode.DebugAdapterDescriptor> {

		if (session.configuration.mode == "connect") {
			let port = session.configuration.fpdserver?.port | 9159;
			let host:string = session.configuration.fpdserver?.host ? session.configuration.fpdserver?.host : "127.0.0.1";
			return new vscode.DebugAdapterServer(port, host);
		} else if (session.configuration.fpdserver?.executable) {
			const command = session.configuration.fpdserver.executable;
			const args = [];
			const options = {
				cwd: session.configuration.fpdserver.cwd,
				env: {}
			}
			return new vscode.DebugAdapterExecutable(command, args, options);
		} else {
		  return executable;
		}
	}

	dispose() {
	}
}

