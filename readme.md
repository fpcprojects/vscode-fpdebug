# VS Code-plugin for fpDebug

This plugin enables debugging Free Pascal applications with fpDebug.

It is essentially a wrapper around [FPDServer](https://gitlab.com/fpcprojects/fpdserver), the debugging-server from fpDebug. This plugin includes an embedded version of FPDServer for 64-bits Windows and Linux.

## Getting started

### Prerequisites

FPDebug only suports x86 and x64 platfoms, running Windows or Linux.

This plugin does not invoke the Free Pascal compiler, the applications has to be compiled already.

It only works with Dwarf debug-information. So you have to compile your applications with the '-gw3' option.

If you are on a 32-bits system or macOS you have to compile FPDServer yourself. You can get it from [here](https://gitlab.com/fpcprojects/fpdserver).

### Use fpDebug

Press ctrl-p (cmd+p on OS X) and run `ext install CNOC.fpdebug` in visual studio code to install this plugin.

Press ctrl-shift-D to switch to the Run-screen. When there are no debuggers already defined, you have to select 'create a launch.json file'. Select 'Pascal (fpDebug)'. When there already is a launch.json file, you have to click on 'Add configuration' and select 'fpDebug: Debug a program'.

![Create a new debug-environment (launch.json) in Visual Studio Code](images/CreateNewDebugEnvironment.png)

![Select the 'Pascal (fpDebug) debugger](images/SelectThePascalDebugger.png)

Change the value of 'program' so that it points to the executable you want to debug. And maybe you want to change 'name' to something you can remember.

![An example debug-configuration (launch.json) for fpDebug](images/ExampleDebugConfiguration.png)

Set the configuration you just made as the active one, and press F5 to start debugging.

![Debugging with fpDebug](images/DebuggingWithFpDebug.png)

### Use parameters, setup the environment and select a working-directory

To pass parameters to the program you want to debug, you have to add those to the debug-configuration (launch.json). The parameters are passed as an array of string.

The working-directory can be set in a similar way using a 'workingdirectory' element.

It is possible to set environment-variables by adding them as name-value combinations to 'environment'.

This is an example on how to set the environment, use program-parameters and to specify a working-directory:

		{
			"configurations": [
				{
					"type": "fpDebug",
					"request": "launch",
					"name": "App with parameters",
					"program": "${workspaceFolder}/testparams",
					"parameters": [
						"First parameter",
						"Second parameter"
					],
					"workingdirectory": "/tmp",
					"environment": [
						{
							"name": "EnvironmentVariable1",
							"value": "Some value"
						},
						{
							"name": "EnvironmentVariable2",
							"value": "Another value"
						}
					]
				}
			]
		}


## Advanced topics

### Select another FPDServer-executable

It is possible to use another FPDServer-executable then the one embedded within this plugin. Just adapt the launch.json and add an fpdserver-section as follows:

	{
		"type": "fpDebug",
		"request": "launch",
		"name": "Launch Program",
		"program": "${workspaceFolder}/executable",
		"fpdserver": {
			"executable": "c:/full/location/and/filename/of/fpdserver.exe"
		}
	}

### Run FPDServer as a standalone server

It is also possible to start an instance of FPDServer and connect to it using tcp/ip. This way it is possible to do remote-debugging, or to debug the FPDServer itself. For this to work the plugin has to be configured to run in the 'connect' mode, and the port and hostname have to be provided in the launch.json.

	{
		"type": "fpDebug",
		"request": "launch",
		"name": "Launch Program",
		"program": "${workspaceFolder}/executable",
		"mode": "connect",
		"fpdserver": {
			"port": 9159,
			"host": "127.0.0.1"
		}
	}

### Create a separate console-window for the debuggee (Windows only)

On Windows there is the posibility to create a new console-window in which the application is started. This can be enabled with the `ForceNewConsoleWin` option:

	{
		"type": "fpDebug",
		"request": "launch",
		"name": "Launch Program",
		"program": "${workspaceFolder}/executable",
		"ForceNewConsoleWin": true
	}

### Change the order of object-members

It is possible to change the order of members of properties. This is done with a `ordervariablesby` section in launch.json. The section should be structured like:

	{
		"type": "fpDebug",
		"request": "launch",
		"name": "Launch Program",
		"program": "${workspaceFolder}/executable",
		"ordervariablesby": [
			{
				"strategy": "inheritencedepth",
				"order": "descending"
			},
			{
				"strategy": "visibility",
			},
			{
				"strategy": "defined",
				"order": "ascending"
			},
			{
				"strategy": "alphabetic"
			}
		]
	}

There can be as any ordervariablesby-entries as you want. Members are first sorted based on the first entry. If two members have the same value for this entry, the second entry is being used, and so on.

There are several properties that can be sorted on: (strategies)

 * defined

   Sort by the order in which the members are defined in the code. Ascending means that members which are defined first come first.

 * visibility

   Sort members by their visibility. Ascending means that private members come first.

 * inheritencedepth

   Sort based on the class the member is defined in. Ascending means that members of parent classes come before child classes.

 * alphabetic

The default is to sort ascending.

### Executable-extention variable

On Windows an application has the `.exe` extension. A special variable `${command:executableExtension}` returns this extension.

### Logging

It is possible to log all the DAB-communication between VS Code and the FPDServer. With the 'log' setting in launch.json set to 'true', a new output-window named 'FpDebug DAB communication' will appear.

## Known issues

This project is not finished yet. These are a few things that do not work as desired. (Patches are always welcome)

 - Debugging threaded applications does not work well
 - The presentation of variables need more work

## Contributing

Questions, ideas and patches are welcome. You can send them directly to <joost@cnoc.nl>, or discuss them at the fpc-pascal mailinglist.

## Versioning

This package is just a basic wrapper around FPDServer and will follow the version-number of the FPDServer it embeds. For the versions available, see the tags on this repository.

## Authors

* **Marc Weustink** - *initial implementation of fpdebug*
* **Joost van der Sluis** - *extended fpdebug, initial implementation of FPDServer and the VS-Code plugin* - <joost@cnoc.nl>
* **Martin Friebe** - *extended fpdebug*
* **Various others**

## License

This extension consists of two parts, with each their own license:

 * The plugin is distributed under the MIT license (see the [LICENSE.txt](LICENSE.txt) file). The code is based on the mock-debug adapter from Microsoft. (see the [thirdpartynotices.txt](thirdpartynotices.txt) file)
 * FPDServer is released under the GNU General Public License, version 2 (GPLv2) (see the [COPYING.GPL.txt](COPYING.GPL.txt) file)